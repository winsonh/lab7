/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef PeriodTimer_H
#define PeriodTimer_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    


void InitPeriodicInt( void );
// void PeriodicIntResponse( void );

#endif /* PeriodTimer_H */

