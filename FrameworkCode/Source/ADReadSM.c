// #define TEST
/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "ADReadSM.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "ADMulti.h"
#include "PWMLib.h"
#include "Interrupt.h"
#include "PeriodTimer.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in Lab3 that are referenced
*/
// #include "LCD_Write.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define ONE_TENTH_SEC (ONE_SEC/10)
#define PE0 0
#define MAX_AD_COUNT 4096
#define NUM_PULSE_PER_REV 512
#define TicksPerMS 1250
#define GEARBOX_RATIO (59/10)
#define MS_PER_TICK 25
#define MS_S_CONVERSION 1000
#define MS_PER_MINUTE 60000000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void InitializeLEDPins(void);
static void UpdateLED(uint8_t GroupNumber);
static uint8_t GetSpeedGroup(uint32_t Period);
// static uint16_t ComputeRPM(uint32_t Period);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t LastGroupNumber = 0;
static ADReadState_t CurrentState = InitPState;
static uint16_t CurrentStep = 0;
static uint16_t DutyCycle = 0;

static ES_Event_t DeferralQueue[3+1];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitFSDriveService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any 
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitADReadSM ( uint8_t Priority )
{
  
  ES_Event_t ThisEvent;
  
  MyPriority = Priority;
  
  // Initialize PE0 as AD Converter input pin
  ADC_MultiInit(4); 

  
  //Put us into the initial pseudo-state to set up for the initial transition
	CurrentState = InitPState;
  
  // Initialize PWM Ports
  InitPWM();
  
  // Initialize Input Capture Interrupt
  InitInputCapturePeriod();
  
  // Initialize Period Timer INterrupt
  InitPeriodicInt();
  
  // Initialize LED pins
  InitializeLEDPins();
  
  // Start 0.1s timer 
  ES_Timer_StopTimer(ADRead_TIMER);
  ES_Timer_SetTimer(ADRead_TIMER, ONE_TENTH_SEC);
  ES_Timer_StartTimer(ADRead_TIMER);
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {  
    return true;
  }else
  {
      return false;
  }
}

/****************************************************************************
 Function
     PostLCDService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostADReadSM( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLCDService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/

ES_Event_t RunADReadSM( ES_Event_t ThisEvent )
{
  struct ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
  
  switch (CurrentState){
    case InitPState :
      /*
      if ( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ADRead_TIMER){
        
        // Set Duty Cycle to be AD read
        // ADC_MultiRead(ADResults);
        // printf("\n\r ADResults = %d", ADResults[PE0]);
        // uint16_t DutyCycle = 100 * (float)ADResults[PE0] / MAX_AD_COUNT;
        
        
        // Start 0.1s timer 
        ES_Timer_StopTimer(ADRead_TIMER);
        ES_Timer_SetTimer(ADRead_TIMER, ONE_TENTH_SEC);
        ES_Timer_StartTimer(ADRead_TIMER);
        
        
        
        uint32_t Period = GetPeriod();
        uint8_t GroupNumber = GetSpeedGroup(Period);
        
        UpdateLED(GroupNumber);

        
        // uint16_t RPM = ComputeRPM(Period); 
        
        // HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT4HI;
        // HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT4LO;
        
        if(CurrentStep < 40) {
          
          if(CurrentStep == 38) {
            printf("\n\rDutyCycle = %d, RPM = %d", DutyCycle, RPM);
          }
        } else {
          CurrentStep = 0;
          DutyCycle = DutyCycle + 1;
          SetDutyCycle(DutyCycle);
          
        }
        CurrentStep++;
        
        SetDutyCycle(50);
        
      }
      */
      break;
      
  }
  
  return ReturnEvent;
}




static void InitializeLEDPins(void) {
  // Initialize Port A, PortB
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0; // Enable Port A
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0){;}
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; // Enable Port B
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1){;}
    
  // Digital Pin Selection
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT4HI | BIT5HI | BIT6HI | BIT7HI); // Set PA4, PA5, PA6, PA7 to be digital
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT4HI); // Set PB0, PB1, PB2, PB3, PB4 to be digital
    
  // Data Direction Setting
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT4HI | BIT5HI | BIT6HI | BIT7HI); // Set PA4, PA5, PA6, PA7 to be output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT4HI); // Set PB0, PB1, PB2, PB3, PB4 to be output
  
    
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); // Set PA4, PA5, PA6, PA7 to be low
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO & BIT4LO); // Set PB0, PB1, PB2, PB3, PB4 to be low 
}



static void UpdateLED(uint8_t GroupNumber) {
  if (LastGroupNumber != GroupNumber) {
    if(GroupNumber == 1 ) {
    HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT3HI);
    }
  
    if(GroupNumber == 2) {
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT2HI | BIT3HI);
    }
  
    if(GroupNumber == 3) {
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI | BIT3HI);
    }
  
    if(GroupNumber == 4) {
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
    }
  
    if(GroupNumber == 5) {
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT4HI;
    }
  
    if(GroupNumber == 6) {
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI | BIT5HI);
    }
  
    if(GroupNumber == 7) {
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI | BIT5HI | BIT6HI);
    }
 
    if(GroupNumber == 8) {
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO & BIT7LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO); 
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
      HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI | BIT5HI | BIT6HI | BIT7HI);
    }
  }
  
  
}



static uint8_t GetSpeedGroup(uint32_t Period) {
  
  if (Period < 16000 ) {
    return 1;
  }
  else if (Period < 19500) {
    return 2;
  }
  else if (Period < 22000) {
    return 3;
  }
  else if (Period < 25500) {
    return 4;
  }
  else if (Period < 30000) {
    return 5;
  }
  else if (Period < 33500) {
    return 6;
  }
  else if (Period < 38000) {
    return 7;
  }
  else {
    return 8;
  }
  // Fastest speed has period 11000
  // Slowest speed has period 40000
}


/*
static uint16_t ComputeRPM(uint32_t Period) {

  int PeriodInMS = Period * MS_PER_TICK / MS_S_CONVERSION; // Period_ticks * (25ns/ tick)* (0.001us/ns)
  int OneRevInMS_Motor = PeriodInMS * NUM_PULSE_PER_REV; 
  int OneRevInMS_Wheel = OneRevInMS_Motor * GEARBOX_RATIO; 
  int RPM = MS_PER_MINUTE / OneRevInMS_Wheel; 
  
  return RPM;
}
*/





















#ifdef TEST
#include <termio.h>

int main(void) {
  TERMIO_Init();
  printf("TEST NOW!\n");
  // InitFSDriveService(MyPriority);
  PWM_TIVA_Init(16);
  PWM_TIVA_SetDuty(50, 0);
  /*
  int i = 0;
  while(!kbhit()) {
    for(int j = 0; j < 100000; j++) {
      ;
    }
    
    if(i >= 16) {
      i = 0;
    }
    
    PWM_TIVA_SetDuty(FS_StepArray[i] , PF0);
    // printf("\n\r%d", FS_StepArray[i]);
    PWM_TIVA_SetDuty(FS_StepArray[i+1] , PF1);
    // printf("\n\r%d", FS_StepArray[i+1]);
    PWM_TIVA_SetDuty(FS_StepArray[i+2] , PF2);
    // printf("\n\r%d", FS_StepArray[i+2]);
    PWM_TIVA_SetDuty(FS_StepArray[i+3] , PF3);
    // printf("\n\r%d\n", FS_StepArray[i+3]);
    
    i = i + 4;
    
    ADC_MultiRead(ADResults);
    printf("\r\nCh0 = %u, Ch1 = %u, Ch2 = %u, Ch3 = %u", ADResults[0], ADResults[1], ADResults[2], ADResults[3]);

  }
  */

  
  while(!kbhit()) {
   for(int j = 0; j < 100000; j++) {
      ;
   }
   CheckSwitchPress();
   printf("\n\rStepInterval = %d", GetStepInterval());
  }
  
  
  
  return 0;
}

#endif

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

