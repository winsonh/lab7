#include <stdint.h>
#include <stdbool.h>
#include "bitdefs.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_pwm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "inc\tm4c123gh6pm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "ADMulti.h"
#include "Interrupt.h"
#include "PWMLib.h"

#define TicksPerMS 1250
#define NUM_AD_INPUT_PORTS 4
#define PE0 0
#define MAX_AD_COUNT 4095
#define MAX_RPM 60
#define NUM_PULSE_PER_REV 512
#define GEARBOX_RATIO (59/10)
#define MS_PER_TICK 25
#define MS_S_CONVERSION 1000
#define MS_PER_MINUTE 60000000
#define Kp 1
#define Kd 0 // 0.001
#define Ki 0.01 //0.0005

static uint32_t TimeoutCount = 0;
static uint16_t LastError = 0;
static uint32_t ADResults[NUM_AD_INPUT_PORTS];
static uint32_t ComputeRPM(uint32_t Period);
static uint8_t GenerateDutyCycle(uint16_t DesiredRPM, uint16_t CurrentRPM);
static int32_t AccumulatedError = 0;
static uint8_t Count = 0;

void InitPeriodicInt( void ){
  // start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1; // kill a few cycles to let the clock get going
  while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R1) != SYSCTL_PRWTIMER_R1){
    ;
  }

  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // set up timer A in periodic mode so that it repeats the time-outs
  HWREG(WTIMER1_BASE+TIMER_O_TAMR) =(HWREG(WTIMER1_BASE+TIMER_O_TAMR)& ~TIMER_TAMR_TAMR_M)|TIMER_TAMR_TAMR_PERIOD;

  // set timeout to 100mS
  HWREG(WTIMER1_BASE+TIMER_O_TAILR) = TicksPerMS * 100;

  // enable a local timeout interrupt
  HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_TATOIM;

  // enable the Timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 96 so appears in EN3 at bit 0
  // HWREG(NVIC_EN2) |= BIT30HI;
  HWREG(NVIC_EN3) |= BIT0HI;
  
  // Change the Period Timer's Priority to 1
  HWREG(NVIC_PRI24) |= BIT6HI; 

  // make sure interrupts are enabled globally
  __enable_irq();
  
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN |
  TIMER_CTL_TASTALL);
  
  
}


void PeriodicIntResponse( void ){
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT4HI;
  
  // start by clearingWTIMER the source of the interrupt
  HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_TATOCINT;
  
  // increment our counter so that we can tell this is running
  ++TimeoutCount;

  ADC_MultiRead(ADResults);
  
  uint16_t DesiredRPM = (uint16_t) ((float) ADResults[PE0] / MAX_AD_COUNT * MAX_RPM) ;
  uint32_t CurrentPeriod = GetPeriod();
  uint32_t CurrentRPM = ComputeRPM(CurrentPeriod);
  uint8_t DutyCycle = GenerateDutyCycle(DesiredRPM, CurrentRPM);
  SetDutyCycle(DutyCycle);
  
  // printf("\n\rHello World!");
  
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT4LO;
  
}



static uint8_t GenerateDutyCycle(uint16_t DesiredRPM, uint16_t CurrentRPM) {
   
  int CurrentError = DesiredRPM - CurrentRPM;
  int CurrentErrorChange = CurrentError - LastError;
  AccumulatedError = AccumulatedError + CurrentError;
  // printf("\n\rCurrentError = %d, AccumulatedError = %d", CurrentError, AccumulatedError);
  
  uint8_t DutyCycle = Kp*CurrentError + Kd*CurrentErrorChange + Ki*AccumulatedError;
  
  
  if(DutyCycle >= 100) {
    DutyCycle = 100;
    
    // For anti-winding
    AccumulatedError = AccumulatedError - CurrentError;
  }
  
  LastError = CurrentError;
  
  return DutyCycle;
  // return 50;
}




static uint32_t ComputeRPM(uint32_t Period) {
  
  uint32_t PeriodInMS = Period * MS_PER_TICK / MS_S_CONVERSION; // Period_ticks * (25ns/ tick)* (0.001us/ns)
  uint32_t OneRevInMS_Motor = PeriodInMS * NUM_PULSE_PER_REV; 
  uint32_t OneRevInMS_Wheel = OneRevInMS_Motor * GEARBOX_RATIO; 
  uint32_t RPM = MS_PER_MINUTE / OneRevInMS_Wheel; 
  
  return RPM;
}
